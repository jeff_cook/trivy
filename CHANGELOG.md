# [0.5.0](https://gitlab.com/jeff_cook/trivy/compare/v0.4.5...v0.5.0) (2020-10-21)


### Features

* update database and save image ([5fe571b](https://gitlab.com/jeff_cook/trivy/commit/5fe571b90014f2747f7133fa34169ceca2c314ee))
