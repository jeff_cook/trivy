# Aqua Security Trivy

GitLab-CI shared pipeline `include:` for <https://github.com/aquasecurity/trivy>.

## Usage

Add this to you `.gitlab-ci.yml` file.

```yaml
include:
  - project: 'jeff_cook/trivy'
    ref: v0.4.5
    file: '/.gitlab-ci/.gitlab-ci.yml'
```

### Ignore the specified vulnerabilities

Use `.trivyignore` to ignore specified vulnerabilities.

<https://github.com/aquasecurity/trivy#ignore-the-specified-vulnerabilities>
